//
//  WebRtcAudioEngine.m
//  voice_engine
//
//  Created by chenzs on 14-1-24.
//
//
#include <stdlib.h>

#import "WebRtcAudioEngine.h"

#import "engine_configurations.h"
#import "common_types.h"
#import "common.h"

#import "webrtc/voice_engine/include/voe_audio_processing.h"
#import "webrtc/voice_engine/include/voe_base.h"
#import "webrtc/voice_engine/include/voe_codec.h"
#import "webrtc/voice_engine/include/voe_dtmf.h"
#import "webrtc/voice_engine/include/voe_encryption.h"
#import "webrtc/voice_engine/include/voe_errors.h"
#import "webrtc/voice_engine/include/voe_external_media.h"
#import "webrtc/voice_engine/include/voe_file.h"
#import "webrtc/voice_engine/include/voe_hardware.h"
#import "webrtc/voice_engine/include/voe_neteq_stats.h"
#import "webrtc/voice_engine/include/voe_network.h"
#import "webrtc/voice_engine/include/voe_rtp_rtcp.h"
#import "webrtc/voice_engine/include/voe_video_sync.h"
#import "webrtc/voice_engine/include/voe_volume_control.h"
#import "webrtc/test/channel_transport/include/channel_transport.h"

#define Debug_Log_Flag      1

//webrtc 名字空间
using namespace webrtc;
using namespace test;

class WebRtcVoeObserver : public VoiceEngineObserver {
public:
    virtual void CallbackOnError(int channel, int err_code);
};

void WebRtcVoeObserver::CallbackOnError(int channel, int err_code) {
    if(Debug_Log_Flag){
        // Add printf for other error codes here
        if (err_code == VE_TYPING_NOISE_WARNING) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  TYPING NOISE DETECTED \n");
        } else if (err_code == VE_RECEIVE_PACKET_TIMEOUT) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RECEIVE PACKET TIMEOUT \n");
        } else if (err_code == VE_PACKET_RECEIPT_RESTARTED) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  PACKET RECEIPT RESTARTED \n");
        } else if (err_code == VE_RUNTIME_PLAY_WARNING) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RUNTIME PLAY WARNING \n");
        } else if (err_code == VE_RUNTIME_REC_WARNING) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RUNTIME RECORD WARNING \n");
        } else if (err_code == VE_SATURATION_WARNING) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  SATURATION WARNING \n");
        } else if (err_code == VE_RUNTIME_PLAY_ERROR) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RUNTIME PLAY ERROR \n");
        } else if (err_code == VE_RUNTIME_REC_ERROR) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RUNTIME RECORD ERROR \n");
        } else if (err_code == VE_REC_DEVICE_REMOVED) {
            NSLog(@"--WebRtcVoeObserver CallbackOnError  RECORD DEVICE REMOVED \n");
        }else{
            NSLog(@"--WebRtcVoeObserver CallbackOnError err_code =%d",err_code);
        }
    }
}


@implementation WebRtcAudioEngine{
    
    VoiceEngine     *_voEngine;
    
    VoEBase         *_base;
    VoECodec        *_codec;
    VoEVolumeControl*_volume;
    VoERTP_RTCP     *_rtp_rtcp;
    VoEAudioProcessing  *_apm;
    VoENetwork          *_netw;
    VoEHardware         *_hardware;
    //VoEVideoSync        *_vsync;
    VoiceChannelTransport *_chanelTransport;
    
    WebRtcVoeObserver   observer;
    
    
    
}

@synthesize isInitinazed = _isInitinazed;

-(void)maybeLog:(NSString*)message{
    if(Debug_Log_Flag){
        NSLog(@"WerbRtcAudioEngine log: %@", message);
    }
}

//param mark create and delete function
-(BOOL)voE_Create{
    if(_voEngine){
        [self maybeLog:@"VoE already created"];
        return NO;
    }
    else{
        _voEngine = VoiceEngine::Create();
        _base  = VoEBase::GetInterface(_voEngine);
        _codec = VoECodec::GetInterface(_voEngine);
        _apm = VoEAudioProcessing::GetInterface(_voEngine);
        _volume = VoEVolumeControl::GetInterface(_voEngine);
        _rtp_rtcp = VoERTP_RTCP::GetInterface(_voEngine);
        _netw = VoENetwork::GetInterface(_voEngine);
        _hardware = VoEHardware::GetInterface(_voEngine);
        _isInitinazed = NO;
        
    }
    [self maybeLog:@"voe created!"];
    return YES;
}

-(BOOL)voE_Delete{
    if(!_voEngine){
        [self maybeLog:@"voe does not created"];
        return NO;
    }
    if(_chanelTransport){
        delete _chanelTransport;
        _chanelTransport = NULL;
    }

    if(_base)
        _base->Release();
    if(_codec)
        _codec->Release();
    if(_apm)
        _apm->Release();
    if(_volume)
        _volume->Release();
    if(_rtp_rtcp)
        _rtp_rtcp->Release();
    if(_netw)
        _netw->Release();
    if(_hardware)
        _hardware->Release();
    
    VoiceEngine::Delete(_voEngine);
    return YES;
}

//Param mark initalization and termination
-(NSInteger)voE_Init{
    int rtn = -1;
    if(_base){
        rtn =_base->Init();
       // observer = new WebRtcVoeObserver();
        _base->RegisterVoiceEngineObserver(observer);
        _isInitinazed = YES;
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_Init:%d", rtn);
    return rtn;
}

-(NSInteger)voE_Terminate{
    int rtn = -1;
    if(_base){
        _base->DeRegisterVoiceEngineObserver();
         rtn = _base->Terminate();
        _isInitinazed = NO;
    }
    if(Debug_Log_Flag)
        NSLog(@"voe_Terminate:%d", rtn);
    return rtn;
}

//param channel function
-(NSInteger)voE_CreateChannel{
    int channel = -1;
    if(_base){
        CodecInst voiceCodec;
        int count = _codec->NumOfCodecs();
        for(int i = 0; i < count; i++){
            if(_codec->GetCodec(i,voiceCodec) != -1){
                if(Debug_Log_Flag)
                    NSLog(@"codecNum:%d, name:%s", i, voiceCodec.plname);
            }
        }
        channel = _base->CreateChannel();
        if(_chanelTransport)
        {
            delete _chanelTransport;
            _chanelTransport = NULL;
        }
        _chanelTransport = new VoiceChannelTransport(_netw, channel);
    }
    if(Debug_Log_Flag)
        NSLog(@"voe_createchannel=%d", channel);
    return channel;
}

-(NSInteger)voe_DeleteChannel:(NSInteger)channel{
    int rtn = -1;
    if(_base){
        rtn = _base->DeleteChannel(channel);
    }
    if(_chanelTransport){
        delete _chanelTransport;
        _chanelTransport = NULL;
    }
    if(Debug_Log_Flag)
        NSLog(@"voe_deletechannel %d",rtn);
    return rtn;
}

//param Receiver and Destination function
-(NSInteger)voE_SetLocalReceiver:(NSInteger)channel localPort:(NSInteger)port{
    int rtn = -1;
    if(_chanelTransport){
        rtn = _chanelTransport->SetLocalReceiver(port);
    }
    if(Debug_Log_Flag)
        NSLog(@"voe_setlocalReceiver %d",rtn);
    return rtn;
}

-(NSInteger)voE_SetSendDestination:(NSInteger)chanel destPort:(NSInteger)port destIp:(NSString*)ipaddr{
    int rtn = -1;
    if(_chanelTransport){
        const char *destIp = [ipaddr cStringUsingEncoding:NSUTF8StringEncoding];
        rtn = _chanelTransport->SetSendDestination(destIp, port);
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_SetSendDestination %d",rtn);
    return rtn;

}

//param mark media functions
-(NSInteger)voE_StartListen:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StartReceive(channel);
    if(Debug_Log_Flag)
        NSLog(@"voe_voE_StartListen %d",rtn);
    return rtn;

}

-(NSInteger)voE_StartPlayout:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StartPlayout(channel);
    if(Debug_Log_Flag)
        NSLog(@"voe_Startplayout %d", rtn);
    return rtn;
}

-(NSInteger)voE_StartSend:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StartSend(channel);
    if(Debug_Log_Flag)
        NSLog(@"voe_StartSend %d", rtn);
    return rtn;
}

-(NSInteger)voE_StopListen:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StopReceive(channel);
    if(Debug_Log_Flag)
        NSLog(@"voE_StopListen %d",rtn);
    return rtn;
}

-(NSInteger)voE_StopPlayout:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StopPlayout(channel);
    if(Debug_Log_Flag)
        NSLog(@"voE_StopPlayout %d",rtn);
    return rtn;
}

-(NSInteger)voE_StopSend:(NSInteger)channel{
    int rtn = -1;
    if(_base)
        rtn = _base->StopSend(channel);
    if(Debug_Log_Flag)
        NSLog(@"voE_StopSend %d",rtn);
    return rtn;
}

//param mark volum and hardware
-(NSInteger)voE_SetSpeakerVolume:(NSInteger)volume{
    int rtn = -1;
    if(_volume)
        _volume->SetSpeakerVolume(volume);
    if(Debug_Log_Flag)
        NSLog(@"voe_setSpeakerVolume %d", rtn);
    return rtn;
}

-(NSInteger)voE_SetLoudSpeakerStatus:(BOOL)enable{
    int rtn = -1;
    if(_hardware)
        _hardware->SetLoudspeakerStatus(enable ? true : false);
    if(Debug_Log_Flag)
        NSLog(@"vOE_SetLoudSpeakerStatus %d", rtn);
    return rtn;

}

//codec-setting functions
-(NSInteger)voE_CountOfCodecs{
    int count = 0;
    if(_codec)
        count = _codec->NumOfCodecs();
    if(Debug_Log_Flag)
        NSLog(@"voE_CountOfCodecs %d", count);
    return count;
}

-(NSArray *)voE_GetCodecs{
    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    if(_codec){
        int count = _codec->NumOfCodecs();
        CodecInst voiceCodec;
        for(int i = 0; i < count; i++){
            if(_codec->GetCodec(i, voiceCodec) != -1){
                NSString *plName = [NSString stringWithUTF8String:voiceCodec.plname];
                [mutableArray addObject:plName];
                if(Debug_Log_Flag)
                    NSLog(@"voE_GetCodecs codecNum:%d, name:%s", i, voiceCodec.plname);
            }
        }
    }
    return mutableArray;
}

-(NSInteger)voE_SetSendCodec:(NSInteger)channel codecIndex:(NSInteger)index{
    int rtn = -1;
    if(_codec){
        CodecInst codecInst;
        if(_codec->GetCodec(index, codecInst) != -1)
          rtn =  _codec->SetSendCodec(channel, codecInst);
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_SetSendCodec %d", rtn);
    return rtn;
}

//param mark voiceEngine function
-(NSInteger)voE_SetECStatus:(BOOL)enable{
    int rtn = -1;
    if(_apm){
        rtn = _apm->SetEcStatus(enable ? true : false);
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_SetECStatus %d", rtn);
    return rtn;
}

-(NSInteger)voE_SetAGCStauts:(BOOL)enable{
    int rtn = -1;
    if(_apm){
        rtn = _apm->SetAgcStatus(enable ? true : false);
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_SetAGCStauts %d", rtn);
    return rtn;

}

-(NSInteger)voE_SetNSSStauts:(BOOL)enable{
    int rtn = -1;
    if(_apm){
        rtn = _apm->SetNsStatus(enable ? true : false);
    }
    if(Debug_Log_Flag)
        NSLog(@"voE_SetNsStauts %d", rtn);
    return rtn;
}


@end
